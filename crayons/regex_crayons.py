import re

SPECIAL_CHARS =  [r'\.', r'\+', r'\*', r'\?', r'\^', r'\$', r'\(', r'\)', r'\[', r'\]', r'\{', r'\}', r'\|', r'\\', r'\-']
SPECIAL_CHAR_DICT = dict(zip([char.replace('\\', '') if char != '\\\\' else '\\' for char in SPECIAL_CHARS], SPECIAL_CHARS))
SPECIAL_CHAR_REGEX = re.compile('|'.join(SPECIAL_CHARS))

def looped_string_replacement(s:str, replacement_dict:dict):
    for key, value in replacement_dict.items():
        s = s.replace(key, value)
    return s

def regex_keyword_dict_replacement(s:str, compiled, keyword_dict:dict, ignore_case:bool=False):
    if ignore_case:
        return compiled.sub(lambda x: keyword_dict.get(x[0].lower(), x[0]), s)
    else:
        return compiled.sub(lambda x: keyword_dict.get(x[0], x[0]), s)

def regex_keyword_combiner(strings:list, left_append:str='', right_append:str='', compile_kwargs:dict={}, compiled=True):
    """useful for combining multiple keywords into regex with a modest amount of terms. Advantage over trie based regex solutions
    in that it examines comminalities in both the start and end of the strings. Disadvantage is initial construction performance
    if the lest of terms becomes very long."""
    
    def level_count(strings:list, reverse:bool):
        if reverse:
            i, step = -1, -1
        else:
            i, step = 0, 1
        min_len = 0
        capped = False
        while capped == False:
            # Initial string created here to avoid additional checks in the inner loop.
            try:
                prev = strings[0][i]
            except IndexError:
                return min_len
            
            for s in strings:
                if len(s) <= min_len:
                    capped = True
                    break
                if s[i] != prev:
                    capped = True
                    break

            i += step
            min_len += 1
        return min_len - 1
    
    def extract_forward(strings:list, depth:int):
        return strings[0][:depth], [s[depth:] for s in strings if s[depth:]]
    
    def extract_backward(strings:list, depth:int):
        return strings[0][-depth:], [s[:-depth] for s in strings if s[:-depth]]
    
    def extract_first(strings:list):
        return [s[:1] for s in strings]
    
    def escape_regex_symbols(s:str):
        return SPECIAL_CHAR_REGEX.sub(lambda x: SPECIAL_CHAR_DICT.get(x[0], x), s)
    
    def single_string_remainder(s:str):
        
        s = escape_regex_symbols(s)

        if len(s) == 1:
            return s + '?'
        else:
            return f'(?:{s})?'
        
    def string_subdivision(strings):
        prev = None
        output = []
        i = -1
        for s in strings:
            curr = s[0]
            if curr != prev:
                output.append([s])
                prev = curr
                i += 1
            else:
                output[i].append(s)
        return output
    
    def regex_recursion(strings:list, child=False, root=False):
        
        if len(strings) == 1 and child:
            return single_string_remainder(strings[0])
        elif len(strings) == 1:
            return escape_regex_symbols(strings[0])
        string_count = len(strings)
        forward_depth = level_count(strings, reverse=False)
        if forward_depth:
            s, strings = extract_forward(strings, forward_depth)
            s = escape_regex_symbols(s)
            if child:
                return f'(?:{s}{regex_recursion(strings, child)})?'
            child = string_count != len(strings)
            return f'{s}{regex_recursion(strings, child)}'
        backward_depth = level_count(strings, reverse=True)
        if backward_depth:
            s, strings = extract_backward(strings, backward_depth)
            child = string_count != len(strings)
            return f'{regex_recursion(strings, child)}{escape_regex_symbols(s)}'
        return '|'.join([regex_recursion(split, child) for split in string_subdivision(strings)]) if root else '(?:' + '|'.join([regex_recursion(split, child) for split in string_subdivision(strings)]) + ')'
    
    strings = sorted(strings)
    regex = regex_recursion(strings, root=True)
    regex = left_append + regex + right_append
    if not compiled:
        return regex
    else:
        return re.compile(regex, **compile_kwargs)